# GPGlue

This project is little more than a README file explaining how to use `gpg` to observe how symmetric and asymmetric encryption work. Maybe in the future I'll use it for more sophisticated examples, but for now, this is all.

See [my blog](https://csb.stevekerrison.com/post/2023-04-gpg-excellent-example-combining-cryptography/ "Steve's Cyber Security Blog {CSB}") for more information.

## Prerequisites

Have GnuPG installed so that you can run `gpg` in a terminal.

## Scenario

There are three users: `alice`, `bob` and `xavier`. User `alice` wants to understand how `gpg` can be used to encrypt a file that can be send to two recipients, `bob` and `xavier`, and it be decryptable by both, efficiently.

## Creating keys

Create a key for `alice`:

```bash
gpg --batch --passphrase "UNSAFE_EXAMPLE" \
  --default-new-key-algo "ed25519/cert,sign+cv25519/encr" \
  --quick-generate-key alice@example.org
 ```

 Then create one for `bob`:

```bash
gpg --batch --passphrase "ANOTHER_UNSAFE_EXAMPLE" \
  --default-new-key-algo "ed25519/cert,sign+cv25519/encr" \
  --quick-generate-key bob@example.org
```

Finally, one for `xavier`:

```bash
gpg --batch --passphrase "REALLY_DON'T_DO_THIS" \
  --default-new-key-algo "ed25519/cert,sign+cv25519/encr" \
  --quick-generate-key xavier@example.org
```

## Encrypt a file

I've used Larry Ewing's rendition of Tux as the source image for this exercise, included as a PNG in this repository.

Encrypt this file from `alice`, to `bob` and `xavier` together:

```bash
gpg -se -u alice@example.org -r bob@example.org -r xavier@example.org \
  -o tux.png.gpg tux.png
```

## Analyse the result

First, the result in full. I am prompted for `bob`'s password when looking into the `.gpg` file because it's the key `gpg` first decides to try to use, and all three keys are maintained locally, so `bob`'s passphrase needs to be used to unlock it (see earlier).

```bash
$ gpg --list-packets --verbose tux.png.gpg
gpg: public key is 241545199596B227
gpg: using subkey 241545199596B227 instead of primary key CD25194DBCA97656
gpg: public key is F24206C42957E8AA
gpg: using subkey F24206C42957E8AA instead of primary key E312D7308A5168A1
gpg: encrypted with 256-bit ECDH key, ID F24206C42957E8AA, created 2023-04-17
      "xavier@example.org"
gpg: using subkey 241545199596B227 instead of primary key CD25194DBCA97656
gpg: encrypted with 256-bit ECDH key, ID 241545199596B227, created 2023-04-17
      "bob@example.org"
gpg: AES256 encrypted data
# off=0 ctb=84 tag=1 hlen=2 plen=94
:pubkey enc packet: version 3, algo 18, keyid 241545199596B227
        data: 40051DA748307DC4AA931A4CBA3A1D9A35F9E473A315672A92A4B0A6244F53DA46
        data: 303A9C3EF1E8715C4B3AE169BC4A1497EF042EBD6CF869265E6FA0C10648FE475914C5DEF002788D017A1BB49CB284FF5E
# off=96 ctb=84 tag=1 hlen=2 plen=94
:pubkey enc packet: version 3, algo 18, keyid F24206C42957E8AA
        data: 40BFB8BE216089247A7F264B529FEE1D4BEBFD303457E585CFEA4C0BB0B0E69D00
        data: 3021FBBD03B11658F7418624B1DCF9F54D47DC0E20B921A8A573F13587CC9F079F5512F1019D93CA0462DB2D2548B421A4
# off=192 ctb=d2 tag=18 hlen=2 plen=0 partial new-ctb
:encrypted data packet:
        length: unknown
        mdc_method: 2
# off=213 ctb=a3 tag=8 hlen=1 plen=0 indeterminate
:compressed packet: algo=2
# off=215 ctb=90 tag=4 hlen=2 plen=13
:onepass_sig packet: keyid 322528C455939965
        version 3, sigclass 0x00, digest 10, pubkey 22, last=1
# off=230 ctb=ad tag=11 hlen=3 plen=11926
:literal data packet:
        mode b (62), created 1681713513, name="tux.png",
        raw data: 11913 bytes
# off=12159 ctb=88 tag=2 hlen=2 plen=136
:signature packet: algo 22, keyid 322528C455939965
        version 4, created 1681713513, md5len 0, sigclass 0x00
        digest algo 10, begin of digest b6 13
        hashed subpkt 33 len 21 (issuer fpr v4 6D73F60FBFD7DACB578FA42E322528C455939965)
        hashed subpkt 2 len 4 (sig created 2023-04-17)
        hashed subpkt 28 len 17 (signer's user ID)
        subpkt 16 len 8 (issuer key ID 322528C455939965)
        data: 48E7BF4267F697DDE74976A2D59608836C17CB6E005B1B5C24E562A434C1B68B
        data: AB7D48537B53A2B742BA2D8A08EB229BDEAAD130D3F44ADED67B62B5B7722C01
```

Now we pick it apart section by section.

### Key information

```
gpg: public key is 241545199596B227
gpg: using subkey 241545199596B227 instead of primary key CD25194DBCA97656
gpg: public key is F24206C42957E8AA
gpg: using subkey F24206C42957E8AA instead of primary key E312D7308A5168A1
```

Two public keys have been found and they are sub-keys (in this case curve25519 [twists](https://cryptobook.nakov.com/asymmetric-key-ciphers/elliptic-curve-cryptography-ecc#curve25519-x25519-and-ed25519) of an Ed25519 primary key).

### Asymmetrically encrypted data

Re-organsising the output a little, as `--list-packets` and `--verbose` are interleaving data:

```
gpg: encrypted with 256-bit ECDH key, ID 241545199596B227, created 2023-04-17
      "bob@example.org"
# off=0 ctb=84 tag=1 hlen=2 plen=94
:pubkey enc packet: version 3, algo 18, keyid 241545199596B227
        data: 40051DA748307DC4AA931A4CBA3A1D9A35F9E473A315672A92A4B0A6244F53DA46
        data: 303A9C3EF1E8715C4B3AE169BC4A1497EF042EBD6CF869265E6FA0C10648FE475914C5DEF002788D017A1BB49CB284FF5E

gpg: encrypted with 256-bit ECDH key, ID F24206C42957E8AA, created 2023-04-17
      "xavier@example.org"
# off=96 ctb=84 tag=1 hlen=2 plen=94
:pubkey enc packet: version 3, algo 18, keyid F24206C42957E8AA
        data: 40BFB8BE216089247A7F264B529FEE1D4BEBFD303457E585CFEA4C0BB0B0E69D00
        data: 3021FBBD03B11658F7418624B1DCF9F54D47DC0E20B921A8A573F13587CC9F079F5512F1019D93CA0462DB2D2548B421A4
```

These two public key encrypted packets contain two data fields each:

1. An ephemeral public key
2. A session key that has been encrypted by a Key Encryption Key (KEK) derived from a Elliptic Curve Diffie Hellman (ECDH) to establish a shared secret between sender and receiver using the aforementioned ephemeral public key and the recipient's public key.

This is a _little_ more complicated with ECC than with RSA, where RSA just encrypts the session key with the public key of the recipient, but the outcome is the same. That's why you'll only see a single data field for RSA-protected GPG messages. Something like this:

```
:pubkey enc packet: version 3, algo 1, keyid DFA443D5D80F73F9
        data: 05D5D9E8111E6559B3237FF65315BD89B7A61CDFE7E7056DBEC8F479030AC9E44C48D2D6C4506F51C30B8EB9F4685B620A11D428A280109EBD8E69DEA5D552DBC4A4DE0499C3F7EF8D9BA3C65A1E567B5638CAC5FA803DE7800A38C579AF7279F9872FB943913DBE6CA82E546B59B87BD55CDF3F6471931A4CC2A3103816B88E4DE1C28125E0D0E938F1ACC2AA60137D03A13503B9697D2AEB0C6C5A17928F82A962D57A6AF2448F7CE563D541FEC2EB02EBF453C7775F977A3FBE780E6B9B2AA52BE236A5A8CE0BCD90BF78B1C398A60FE35359501AC1BECCEE4F0AFD134A9AC6D4FD7907C6BDFBFB84B07A8F1CA42FDCD69AA049874592638013025D60A143578D5FA074C2DA43B9CC9343468681BED68DB0A655A8DD8E0E22EB5E888A26DF64F485A7E2AE2AF2E528A2098D8BACFEC430666B4413E5F2C702E7156570335FAE693F450DF9F509F9306B889AAF44A431DC0D7C8906055A1ADBF990729833E84030CDC7A6A32B036AED4BD9F25233A88D3ADBC02D0ED2E615E34475FC82C614
```

The specifications for these two approaches are defined in [RFC 6637 &#xA7;8 for ECC](https://www.rfc-editor.org/rfc/rfc6637.html#section-8 "Elliptic Curve Cryptography (ECC) in OpenPGP - EC DH Algorithm (ECDH)") and [RFC 4880 &#xA7;5.1 for RSA](https://www.rfc-editor.org/rfc/rfc4880.html#section-5.1 " OpenPGP Message Format - Public-Key Encrypted Session Key Packets").

### Symmetrically encrypted data

On now to the data we actually care about: a penguin. Again, reorganising the output slightly, we have:

```
gpg: AES256 encrypted data
# off=192 ctb=d2 tag=18 hlen=2 plen=0 partial new-ctb
:encrypted data packet:
        length: unknown
        mdc_method: 2
```

We can see that this is AES256 encrypted data. `mdc_method` relates to modification detection codes, also explained in RFC 4880.

After decrypting this packet, we find another type of packet inside.

### Compressed data

```
# off=213 ctb=a3 tag=8 hlen=1 plen=0 indeterminate
:compressed packet: algo=2
```

There's not much to say about this one, other than that both encrypted and compressed packets must themselves contain valid OpenPGP messages. Valid message are encrypted or compressed messages, literal data, or signed messages. So, the encrypted packet contained a compressed packet, which itself contains...

### Signed message

Our signed message comprises three parts:

1. A _onepass_ packet that describes the algorithm(s) used in signature generation, so that when sequentially reading a large file, we can start hashing the data straight away.
2. The literal data packet: The thing we actually encrypted. You can see its original name encoded into the packet header.
3. The signature packet, which contains a signature of the data packet, signed by the sender's public key.


```
# off=215 ctb=90 tag=4 hlen=2 plen=13
:onepass_sig packet: keyid 322528C455939965
        version 3, sigclass 0x00, digest 10, pubkey 22, last=1
# off=230 ctb=ad tag=11 hlen=3 plen=11926
:literal data packet:
        mode b (62), created 1681713513, name="tux.png",
        raw data: 11913 bytes
# off=12159 ctb=88 tag=2 hlen=2 plen=136
:signature packet: algo 22, keyid 322528C455939965
        version 4, created 1681713513, md5len 0, sigclass 0x00
        digest algo 10, begin of digest b6 13
        hashed subpkt 33 len 21 (issuer fpr v4 6D73F60FBFD7DACB578FA42E322528C455939965)
        hashed subpkt 2 len 4 (sig created 2023-04-17)
        hashed subpkt 28 len 17 (signer's user ID)
        subpkt 16 len 8 (issuer key ID 322528C455939965)
        data: 48E7BF4267F697DDE74976A2D59608836C17CB6E005B1B5C24E562A434C1B68B
        data: AB7D48537B53A2B742BA2D8A08EB229BDEAAD130D3F44ADED67B62B5B7722C01
```

We can see that `keyid 322528C455939965` does not match any of the keys mentioned earlier in the dissection, but if we list the keys on our machine, we will surely find who it belongs to:

```bash
$ gpg --list-keys --keyid-format LONG 
# [...]
pub   ed25519/322528C455939965 2023-04-17 [SC] [expires: 2025-04-16]
      6D73F60FBFD7DACB578FA42E322528C455939965
uid                 [ultimate] alice@example.org
sub   cv25519/7A815E449C697F34 2023-04-17 [E]
# [...]
```

Indeed, it's `alice`'s key, as we would expect. Note that the Ed25519 key is used, rather than the curve25519 sub-key, because Ed25519 keys are suitable for use in signing algorithms rather than encryption, as indicated by the `S` in square brackets.

## Summary

This project has provided simple instructions for preparing some GPG keys and using them to encrypt a file for multiple recipients in a single output file. It has then walked through a dissection of the output file, using `gpg`, explaining the important steps. It has shown how OpenPGP is designed to efficiently and securely share encrypted data, with a combination of public-key asymmetric cryptography and symmetric cryptography.

If you find any issues reproducing these results, or contradictions with other information sources, feel free to open an issue or propose a fix.